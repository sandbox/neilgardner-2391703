<?php

function useragent_geolocation($lat = 0,$lng = 0,$full=true,$json = true) {
	$lat = useragent_floatonly($lat);
	$lng = useragent_floatonly($lng);
	if (!useragent_fetch_locality()) {
		$full = false;
	}
	if ($lat < -90) {
		$lat = -90;
	} else if ($lat > 90) {
		$lat = 90;
	}
	if ($lng < -180) {
		$lng = -180;
	} else if ($lat > 180) {
		$lat = 180;
	}
	$cacheKey = 'useragent:geodata_'.$lat . '_' . $lng;
	if (USER_AGENT_CACHE_GEO==1 && $cache = cache_get($cacheKey)) {
		$data = $cache->data;
	}
	else {
		$data = new StdClass;
		$data->valid = false;
		$data->code = 'xx';
		$data->coords = array('lat' => $lat, 'ng' => $lng);
		$data->hasLocality = false;
		if (!$full) {
			$url = useragent_geonames_url() . '?lat=' . $lat . '&lng=' . $lng;
			$countryCode = @file_get_contents($url);
			if (is_string($countryCode) && strlen($countryCode) > 1 && strlen($countryCode) < 6) {
				$data->valid = true;
				$data->code = trim($countryCode);
				useragent_add_macroregion($data);
			}
		} else {
			$params = array(
					'@lat' => $lat,
					'@lng' => $lng,
					'@username' => useragent_geonames_api_username(),
			);
			$url = t(USERAGENT_GEONAMES_API,$params);
			$strData = @file_get_contents($url);
			$geoData = NULL;
			if (!empty($strData) && preg_match('#^\s*\{#',$strData)) {
				$geoData = json_decode($strData);
			}
			if (is_object($geoData) && isset($geoData->geonames) && is_array($geoData->geonames) && isset($geoData->geonames[0]) && is_object($geoData->geonames[0])) {
				foreach ($geoData->geonames as $geo) {
					if (isset($geo->adminName2)) {
						$geo = $geoData->geonames[0];
						$data->valid = true;
						$data->code = trim($geo->countryCode);
						if (isset($geo->toponymName) && !empty($geo->toponymName)) {
							$data->locality = trim($geo->toponymName);
						} else if (isset($geo->adminName3) && useragent_valid_placename($geo->adminName3)) {
							$data->locality = trim($geo->adminName3);
						}
						if (isset($geo->adminName2) && useragent_valid_placename($geo->adminName2)) {
							$data->city = trim($geo->adminName2);
						} else if (isset($geo->adminName1) && useragent_valid_placename($geo->adminName1)) {
							$data->city = trim($geo->adminName1);
						}
						if (empty($data->city)) {
							$data->city = $data->locality;
						}
						$data->hasLocality = (!empty($data->city));
						$data->timezone = $geo->timezone;
						useragent_add_macroregion($data);
						break;
					}
				}
			}
		}
		$expire = time() + 7* (24*60*x60);
		cache_set($cacheKey,$data,'cache',$expire);
	}
	useragent_add_sunrise($data);
	if ($json) {
		print drupal_json_output($data);
	} else {
		return $data;
	}
}

function useragent_server_data_json() {
	$data = new StdClass;
	$d = useragent_strings();
	$d = array_merge($d,useragent_strings('iPad',10));
	$d = array_merge($d,useragent_strings('Android',10));
	$d = array_merge($d,useragent_strings('Kindle',5));
	$rows = array();
	module_load_include('inc', 'useragent','useragent.server');
	foreach ($d as $string) {
		$rows[] = user_agent_parse($string,'en-GB');
	}
	var_dump($rows);
	$data = useragent_server_data();
	print drupal_json_output($data);
}


function useragent_strings($match = NULL,$max = 20) {
	$q = db_select('agentstrings','a')->fields('a',array('name'));
	if (!empty($match)) {
		$q->condition('a.name',$match,'REGEXP');
	}
	$q->orderBy('RAND()');
	$q->range(0,$max);
	$r = $q->execute();
	$data = array();
	if ($r) {
		$data = $r->fetchCol();
		if (!is_array($data)) {
			$data = array();
		}
	}
	return $data;
}

function useragent_valid_placename($string = NULL) {
	return (preg_match('#[a-z][a-z]+#i',$string) && !preg_match('#[a-z]\.\d+#i',$string));
}


function useragent_add_macroregion($data) {
	$meta = useragent_country($data->code);
	$data->name = $meta->name;
	$data->regcode = $meta->regcode;
	$data->macroregion = $meta->macroregion;
}

function useragent_floatonly($string = NULL) {
	$num = preg_replace('[^0-9\.-]','',$string);
	return (float) number_format($num,2);
}

function useragent_ip_geo() {
	$ip = isset($_SERVER['REMOTE_ADDR'])? $_SERVER['REMOTE_ADDR'] : '';
	$ipData = new StdClass;
	$ipData->valid = false;
	$ipRgx = '#\d+\.\d+\.\d+\.\d+#';
	if (!empty($ip) && $ip != '127.0.0.1' && preg_match('#\d+\.\d+\.\d+\.\d+#',$ip)) {
		$cacheKey = 'useragent:ip_geo_'.str_replace('.','_',$ip);
		if (USER_AGENT_CACHE_GEO==2 && $cache = cache_get($cacheKey)) {
			$ipData = $cache->data;
		} else {
			$ipData = _useragent_ip_geo($ip);
			$expire = time() + 7* (24*60*x60);
			cache_set($cacheKey,$ipData,'cache',$expire);
		}
	}
	print drupal_json_output($ipData);
}

function _useragent_ip_geo($ip) {
	$url = t(useragent_geoplugin_url(),array('@ip' => $ip));
	$strData = file_get_contents($url);
	$ipData->ip = $ip;
	$ipData = new StdClass;
	$ipData->valid = false;
	$ipData->hasLocality = false;
	if (!empty($strData)) {
		$data = @unserialize($strData);
		if (is_array($data) || is_object($data)) {
			$data = (object) $data;
			// map of geoplugin keys with useragent api equivalents
			$keyMap = array(
					'geoplugin_countryCode' => 'code',
			);
			foreach ($keyMap as $sourceKey => $targetKey) {
				if (isset($data->{$sourceKey}) && is_string($data->{$sourceKey})) {
					$val = trim($data->{$sourceKey});
					if ($targetKey == 'code') {
						$val = strtoupper(substr($val,0,2));
					}
					$ipData->{$targetKey} = $val;
				}
			}
			$ipData->valid = (isset($data->geoplugin_latitude) && isset($data->geoplugin_longitude) && is_numeric($data->geoplugin_latitude) && is_numeric($data->geoplugin_longitude));
		}
	}
	if ($ipData->valid) {
		$ipData->coords = array(
			'lat' => useragent_floatonly($data->geoplugin_latitude),
			'lng' => useragent_floatonly($data->geoplugin_longitude)
		);
		if (useragent_fetch_locality()) {
			$extraData = useragent_geolocation($data->geoplugin_latitude,$data->geoplugin_longitude,true,false);
			if (is_object($extraData) && $extraData->valid) {
				foreach ($extraData as $key => $value) {
					if (!isset($ipData->{$key})) {
						$ipData->{$key} = $value; 
					}
				}
			}
		}
	}
	return $ipData;
}


function useragent_add_sunrise($data) {
	$zenith = 90+50/60;
	if (isset($data->timezone->timeZoneId)) {
		$tz = explode('/',$data->timezone->timeZoneId);
		$offset = 0;
		if (count($tz)==2) {
			$tzReg = strtolower($tz[0]);
			$tzCity = strtolower($tz[1]);
			$day = date("z");
			switch ($tzReg) {
				case 'europe':
				case 'america':
				case 'atlantic':
					if ($lat > 15 && $day > 80 && $day < 290) {
						$offset = $data->timezone->dstOffset - $data->timezone->gmtOffset;
					}
					break;
				case 'australia':
				case 'pacific':
					if ($lat > 15 && $day > 270 && $day < 95) {
						$offset = $data->timezone->gmtOffset-$data->timezone->dstOffset;
					}
					break;
			}
		}
	}
	$data->sunrise = date_sunrise(time(),SUNFUNCS_RET_STRING,$data->coords['lat'],$data->coords['lng'],$zenith,$offset);
	$data->sunset = date_sunset(time(),SUNFUNCS_RET_STRING,$data->coords['lat'],$data->coords['lng'],$zenith,$offset);
}