(function($) {
	
    Drupal.useragent = {
		settings: Drupal.settings.useragent,
		
		matchScreenSize: function(resetSize) {
			var s = this.settings, matchedSize = 'medium', matched = false, prevSize;
    		if (resetSize===true) {
    			s.browser.height = $(window).height();
    			s.browser.width = $(window).width();
    		}
    		if (s.screensizes) {
    			$.each(s.screensizes, function(size, dims) {
				if (matched == false && dims.length == 2 && dims[0] >= s.browser.width && dims[1] >= s.browser.height) {
					matchedSize = prevSize;
					matched = true;
	    		   }
					prevSize=size;
	    		});
    		}
    		
    		s.browser.orientation = s.browser.width > s.browser.height ? 'landscape' : 'portrait';
    		s.browser.size = matchedSize;
    	},
    	
    	matchSequence: function(text, sequence) {
    		var seqLength = sequence.length,i=0,m = 'other';
    		for (; i < seqLength;i++) {
    			m = text.match(new RegExp('\\b'+sequence[i]+'\\b','i'));
    			if (m != null && m.length>0) {
    				return m.shift().toString();
    			}
    		}
    		return m;
    	},

    	browserData: function() {
    		var b = this.settings.browser;
    		b.platform = this.matchSequence(navigator.userAgent,Drupal.settings.useragent.platforms);
    		b.language = navigator.language;
    		if ( /Android/i.test(b.platform) ) { 
	            if ( !/mobile/i.test(navigator.userAgent) ) { 
	            	b.deviceType = 'tablet'; 
	            } else { 
	            	b.deviceType = 'phone'; 
	            } 
	            b.osVersion = (navigator.userAgent).match(/Android\s+([\d\.]+)/i); 
	            b.osVersion = b.osVersion[0]; 
	            b.osVersion = b.osVersion.replace('Android ', ''); 
	        
	        } else if ( /iPhone/i.test(b.platform) ) { 
	        	b.deviceType = 'phone'; 
	            b.osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i); 
	            b.osVersion = b.osVersion[0]; 
	            b.osVersion = b.osVersion.replace(/_/g, '.'); 
	            b.osVersion = b.osVersion.replace('OS ', ''); 
	        
	        } else if ( /iPad/i.test(b.platform) ) { 
	        	b.deviceType = 'tablet'; 
	            b.osVersion = (navigator.userAgent).match(/OS\s+([\d\_]+)/i); 
	            b.osVersion = b.osVersion[0]; 
	            b.osVersion = b.osVersion.replace(/_/g, '.'); 
	            b.osVersion = b.osVersion.replace('OS ', ''); 
	        } else if ( /(Windows|Mac|Ubuntu|Linux)/i.test(b.platform) ) { 
	        	b.deviceType = 'desktop'; 
	            b.osVersion = (navigator.userAgent).match(/(Windows(\s+(XP|NT)?)|Ubuntu|Linux|OS(\s+X)?)?\s+([\d\_]+|\w+)/i); 
	            if (b.osVersion.length>0) {
	            	b.osVersion = b.osVersion[0]; 
		            b.osVersion = b.osVersion.replace(/_/g, '.'); 
		            b.osVersion = b.osVersion.replace(/OS(\s+X)?/i, '');
	            }
	        }
    		b.name = this.matchSequence(navigator.userAgent,Drupal.settings.useragent.browser_types);
            if (b.name) {
            	if (b.name.length>1) {
                	b.name = b.name.toString().split(',').shift();
                }
            } else {
            	b.name = 'IE 11';
            }
            if (b.platform.length > 1) {
	        	b.platform = b.platform.toString().split(',').shift();
	        }
            b.version = $.browser.version;
            this.matchScreenSize();
    		return b;
    	},
		
		
		get: function(first,second) {
			var s = this.settings;
			if (typeof first == 'string') {
				if (typeof second == 'string') {
					return s[first][second];
				} else {
					return s[first];
				}
			}
		},
		
		write: function() {
			var s = Drupal.settings.useragent;
			if (s.browser.size) {
				s.body.addClass('screen-'+ s.browser.size);
			}
			if (s.browser.deviceType && s.body.hasClass('device-detected')==false) {
				s.body.addClass('device-type-'+ s.browser.deviceType);
			}
			if (s.browser.name && s.match_browser) {
				s.body.addClass('browser-'+ s.browser.name.toLowerCase());
			}
			if (s.browser.platform && s.match_browser) {
				s.body.addClass('platform-'+ s.browser.platform.toLowerCase());
			}
			if (s.browser.orientation) {
				s.body.addClass('orientation-' + s.browser.orientation);
			}
			if (s.hasGeo) {
				s.body.addClass('macroregion-'+ s.geo.regcode.toLowerCase());
				s.body.addClass(s.geo.sunmode);
				s.body.addClass('country-'+ s.geo.code.toLowerCase());
			}
			if (s.time) {
				s.body.addClass('time-'+ s.time);
			}
		},
		
		setScreenSizeClass: function() {
			var s = this.settings;
			this.matchScreenSize(true);
			if (s.browser.size.length>0) {
				var aClasses = $('body').attr('class').split(' '), rgx = new RegExp('^screen-');
				var nC = aClasses.length, i = 0;
				if (nC > 0) {
					for (; i < nC;i++) {
						if (rgx.test(aClasses[i])) {
							$('body').removeClass(aClasses[i]);
							break;
						}
					}
				}
				s.browser.size = sizeClass;
				$('body').addClass('screen-'+sizeClass);
			}
		},
		
		store: function() {
			var s = this.settings, d=new Date(), data = {};
			d.setDate(d.getDate() + 1);
			data.browser = s.browser;
			data.geo = s.geo;
			var cVal=escape(JSON.stringify(data) ) + ((d==null) ? "" : "; expires="+d.toUTCString());
			document.cookie=s.cookiename + "=" + cVal;
		},
		
		read: function() {
			var s = this.settings, cVals = document.cookie, cStart = cVals.indexOf(" " + s.cookiename + "=");
			if (cStart == -1) {
			  cStart = cVals.indexOf(s.cookiename + "=");
			}
			if (cStart == -1) {
		  	cVals = "";
		  } else {
		  	cStart = cVals.indexOf("=", cStart) + 1;
		  	var cEnd = cVals.indexOf(";", cStart);
		  	if (cEnd == -1) {
					cEnd = cVals.length;
				}
				cVals = unescape(cVals.substring(cStart,cEnd));
			}
			if (cVals.length>0) {
				return JSON.parse(cVals);
			}
		},
		
		toMinutesOfDay: function(timeStr,defMins) {
			if (/^\d+:\d+$/.test(timeStr)) {
				var parts = timeStr.split(':');
				return ((parts[0]-0)*60) + (parts[1] -0);
			}
			return defMins;
		},
		
		setTimeParams: function() {
			var s = this.settings;
			this.setTimeOfDay();
			if (s.hasGeo && s.geo && s.geo.sunrise) {
				s.minutes = s.date.getMinutes();
				var riseMins = this.toMinutesOfDay(s.geo.sunrise,360),
				setMins = this.toMinutesOfDay(s.geo.sunset,1080),
				minOfDay = (s.hour * 60) + s.minutes;
				s.geo.sunmode = (minOfDay > riseMins && minOfDay < setMins)? 'sun-up' : 'sun-down';
			}
			// make configurable
		},
		
		setTimeOfDay: function(updateBody) {
			var s = this.settings, prevPeriod = '', tIndex = 0, matched = false, matchedStart = false;
			s.time = '';
			s.date = new Date();
			s.hour = s.date.getHours();
			$.each(s.timesofday, function(period, stHr) {
				if (!matchedStart) {
					matchedStart = s.hour >= stHr;
				}
				if (matched != true && s.hour < stHr && matchedStart) {
					s.time = prevPeriod;
					matched = true;
	    		}
				prevPeriod=period;
				tIndex++;
    		});
			if (s.time.length < 1) {
				s.time = prevPeriod;
			}
			if (updateBody && s.time.length>1) {
				if ($('body').attr('class') != 'undefined') {
					var cls = $('body').attr('class').split(' '),i=0;
					for (;i<cls.length;i++) {
						if (/^time-/.test(cls[i])) {
							cls[i] = 'time-'+s.time;
							break;
						}
					}
					s.body.attr('class', cls.join(' '));
				}
			}
		},
		
		init: function() {
			this.settings = Drupal.settings.useragent;
			var s = this.settings, u = this.read();
			s.body = $('body:first');
			if (typeof u == 'object' && u.browser && u.geo) {
				s.geo = u.geo;
				s.hasGeo = (u.geo.code && u.geo.code.length == 2 && u.geo.code != 'xx');
				s.browser = u.browser;
				this.matchScreenSize(true);
				this.setTimeParams();
				this.write();
			} else {
				s.browser = {
					osVersion: 'unknown', 
			        platform:  'unknown',
			        deviceType: 'unknown',
			        name: 'unknown',
				};
				s.hasGeo = false;
				if (s.body.hasClass('device-detected')) {
					if (s.body.hasClass('device-type-tablet')) {
						s.browser.deviceType = 'tablet';
					} else if (s.body.hasClass('device-type-phone')) {
						s.browser.deviceType = 'phone';
					} else {
						s.browser.deviceType = 'desktop';
					}
				}
				if (s.browser.deviceType == 'unknown' || s.match_browser) {
					this.browserData();
				}
		        
				if (s.geo_enabled == 1) {
					Drupal.geo.init();
				} else {
					s.geo = {};
					this.store();
					this.write();
					this.setTimeParams();
				}
			}
        }
	};
	
    Drupal.behaviors.useragent = {
    	attach: function(context) {
    		Drupal.useragent.init()
    	}
    }
})(jQuery);