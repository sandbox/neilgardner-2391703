(function($) {

Drupal.geo = {
	
	showLocation: function (position) {
	  var s = Drupal.settings.useragent; 
	  s.geo = {};
	  s.geo.error = "";
	  s.geo.coords = {lat:position.coords.latitude,lng:position.coords.longitude};
	  var href = s.geo_callback + '/' + position.coords.latitude + '/' + position.coords.longitude;
	  $.ajax({
			url: href,
			success: function(data) {
				var s = Drupal.settings.useragent; 
				s.geo = data;
				s.hasGeo = true;
				Drupal.useragent.write();
				Drupal.useragent.store();
				Drupal.useragent.setTimeParams();
			}
	  });
	},

	errorHandler: function(err) {
		 var s = Drupal.settings.useragent; 
	  switch (err.code) {
		  case 1:
			  s.geo.error = "Error: Access is denied!";
			  break;
		  case 2:
			  s.geo.error = "Error: Position is unavailable!";
			  break;
	  }
	  Drupal.useragent.write();
	},
	
	fetchGeo: function() {
		$.ajax({
			url: Drupal.settings.useragent.ip_callback,
			success: function(data) {
				if (data.valid) {
					var s = Drupal.settings.useragent; 
					s.geo = data;
					s.hasGeo = true;
					Drupal.useragent.setTimeParams();
					Drupal.useragent.store();
					Drupal.useragent.write();
				} else {
					if (navigator.geolocation) {
						navigator.geolocation.getCurrentPosition(Drupal.geo.showLocation,Drupal.geo.errorHandler);
					}
				}
			}
	  });
	},
	
	init: function() {
		var s = Drupal.settings.useragent;
		s.geo = {
    		city: 'unknown',
			name: 'unknown',
			code: 'xx',
        	sunmode: '',
        };
		this.fetchGeo();
	}
};

})(jQuery);