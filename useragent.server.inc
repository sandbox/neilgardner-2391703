<?php

function _useragent_server_data() {
	$agentString = $_SERVER['HTTP_USER_AGENT'];
	$language = strtok(strtok($_SERVER['HTTP_ACCEPT_LANGUAGE'],';'), ',');
	return user_agent_parse($agentString,$language);
}

function user_agent_parse($agentString = NULL, $language = NULL) {
	$browser = new stdClass;
		$platforms = useragent_platforms();
		array_unshift($platforms,'Android');
		$browser->platform = useragent_match_sequence($agentString,$platforms);
		$browser->language = $language;
		if ( preg_match('#Android#i',$browser->platform) ) {
			if ( !preg_match('#mobile#i',navigator.userAgent) ) {
				$browser->deviceType = 'tablet';
			} else {
				$browser->deviceType = 'phone';
			}
			preg_match('/Android\s+([\d\.]+)/i', $agentString,$match);
			if (is_array($match) && !empty($match[1])) {
				$browser->osVersion = preg_replace('#Android\s+#i', '',$match[1]);
			}
			 
		} else if ( preg_match('#iPhone#i',$browser->platform) ) {
			$browser->deviceType = 'phone';
			if (preg_match('/OS\s+([\d\_]+)/i',$agentString,$match)) {
				$browser->osVersion = preg_replace('#_#', '.',$browser->osVersion);
				$browser->osVersion = preg_replace('#OS\s+#', '',$browser->osVersion);
			}
			 
		} else if ( preg_match('#iPad#i',$browser->platform) ) {
			$browser->deviceType = 'tablet';
			if (preg_match('/OS\s+([\d\_]+)/i', $agentString,$match)) {
				$browser->osVersion = $match[1];
				$browser->osVersion = preg_replace('#_#', '.',$browser->osVersion);
				$browser->osVersion = preg_replace('OS ', '',$browser->osVersion);
			}
		} else if ( preg_match('#(Windows|Mac|Ubuntu|Linux)#i',$browser->platform) ) {
			$browser->deviceType = 'desktop';
			if (preg_match('/(Windows(\s+(XP|NT)?)|Ubuntu|Linux|OS(\s+X)?)?\s+([0-9]+([\._][0-9])*)/i',$agentString,$match)) {
				$browser->osVersion = $match[5];
				$browser->osVersion = preg_replace('#_#', '.',$browser->osVersion);
			}
		}
		$browsers = useragent_browser_types();
		$extraBrowsers = array('Netscape','Elinks','Lynx','Iceweasel','Seamonkey','Minefield');
		$browsers = array_merge($browsers,$extraBrowsers);
		$browser->name = useragent_match_sequence($agentString,$browsers);
		$browser->version = NULL;
		
		if (preg_match('#\bversion[^0-9]{0,5}([0-9]+(\.[0-9]+)*)\b#i',$agentString,$match)) {
			$browser->version = $match[1];
		} else if (preg_match('#\b'.$browser->name.'[^0-9]{0,5}([0-9]+(\.[0-9]+)*)\b#i',$agentString,$match)) {
			$browser->version = $match[1];
		}
		if (strlen($browser->name) < 2) {
			$browser->name = 'Other';
		}
		if (strlen($browser->name) >1) {
			$browser->name = trim($browser->name);
		}
		if (strlen($browser->platform) > 1) {
			$browser->platform = trim($browser->platform);
		}
		if ($browser->platform == 'Android' && $browser->name == 'Safari') {
			$browser->name = 'Webkit';
		}
		$browser->string = $agentString;
		return $browser;
}

function useragent_match_sequence($agentString = NULL,$sequence) {
	$m = NULL;
	if (is_array($sequence)) {
		$seqLength = count($sequence);
		foreach ($sequence as $platform) {
			preg_match('#\\b('.$platform.')\b#i',$agentString,$match);
			if (!empty($match[1]) && strlen($match[1])>0) {
				return $match[1];
			}
		}
	}
  return $m;
}