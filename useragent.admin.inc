<?php

$settingsItem['title'] = t("Macro-regions");
$settingsItem['type'] = MENU_LOCAL_TASK;
$settingsItem['page arguments'] = array('useragent_settings_macroregions');
$settingsItem['weight'] = 1;
$items[USERAGENT_ADMIN_PATH.'/countries'] = $settingsItem;


function useragent_settings_overview($form,&$form_state) {
	drupal_set_title("User Agent Customisation Settings");
	$form = array(
			'#submit' => array('useragent_settings_overview_submit')
	);
	
	$defaultTheme = variable_get('theme_default',NULL);
	$defaultEnabledThemes = !empty($defaultTheme)? array($defaultTheme) : array();
	$enabledThemes = variable_get('useragent_enabled_themes',$defaultEnabledThemes);
	$adminTheme = variable_get('admin_theme',NULL);
	$data = list_themes();
	foreach ($data as $key => $theme) {
		if ($theme->status > 0 ) {
			$extra = '';
			if ($key == $defaultTheme) {
				$extra .= ' (default)';
			} else if ($key == $adminTheme) {
				$extra .= ' (admin)';
			}
			$themes[$key] = $theme->name.$extra;
		}
	}
	
	$form['useragent_enabled_themes'] = array(
			'#type' => 'checkboxes',
			'#title' => 'Enabled themes',
			'#options' => $themes,
			'#default_value' => $enabledThemes,
			'#description' => t("Enabled user agent script on these themes.")
	);
	
	$geoEnabled = variable_get('useragent_geo_enabled',true);
	$form['useragent_geo_enabled'] = array(
			'#type' => 'checkbox',
			'#title' => 'Enable Geolocation detection',
			'#description' => t("If disabled the useragent.js script will only gather browser, platform, device type and screen size data")
	);
	if ($geoEnabled) {
		$form['useragent_geo_enabled']['#attributes'] = array('checked' => true);
	}
	
	
	$matchBrowser = variable_get('useragent_match_browser',true);
	$form['useragent_match_browser'] = array(
			'#type' => 'checkbox',
			'#title' => 'Detect and match browser type and version',
			'#description' => t("If disabled only the device type (desktop, tablet or phone), initial orientation and screen-size data will be added to the body class")
	);
	if ($matchBrowser) {
		$form['useragent_match_browser']['#attributes'] = array('checked' => true);
	}
	
	
	/*$varName = 'useragent_output_available_browsers_and_platforms';
	$enabled = variable_get($varName,false);
	$form[$varName] = array(
			'#type' => 'checkbox',
			'#title' => 'Add available browsers and platforms to JS settings (Drupal.settings.useragent object)',
			'#description' => t("Enable this if another custom or contrib module needs this data")
	);
	if ($enabled) {
		$form[$varName]['#attributes'] = array('checked' => true);
	}*/
	
	
	$varName = 'useragent_use_minified';
	$enabled = variable_get($varName,true);
	$form[$varName] = array(
			'#type' => 'checkbox',
			'#title' => 'Use minified javascript',
			'#description' => t("If enabled, the minified javascript file will be used (useragent.min.js). Disable for debugging.")
	);
	if ($enabled) {
		$form[$varName]['#attributes'] = array('checked' => true);
	}
	
	
	$form['save'] = array(
			'#type' => 'submit',
			'#value' => 'Save',
			'#id' => 'edit-save'
	);
	return $form;
}

function useragent_settings_overview_submit($form,&$form_state) {
	$values = $form_state['values'];
	$countries = $values['countries'];
	if (isset($values['useragent_enabled_themes']) && is_array($values['useragent_enabled_themes'])) {
		$values = array_values($values['useragent_enabled_themes']);
		$enabledThemes = array();
		foreach ($values as $v) {
			if (is_string($v) && strlen($v) > 1 && preg_match('#[a-z]+#i',$v)) {
				$enabledThemes[] = $v;
			}
		}
		variable_set('useragent_enabled_themes', $enabledThemes);
	}
	
	$geoEnabled = (int) $values['useragent_geo_enabled'];
	variable_set('useragent_geo_enabled', $geoEnabled);
	
	$matchBrowser = (int) $values['useragent_match_browser'];
	variable_set('useragent_match_browser', $matchBrowser);
	
	
	$msg = "Browser type and version matching has been ";
	if ($matchBrowser) {
		$msg .= "enabled";
	} else {
		$msg .= "disabled";
	}
	drupal_set_message($msg);
	
	$minifEnabled = (int) $values['useragent_use_minified'];
	variable_set('useragent_use_minified', $minifEnabled);
	$msg = "Geolocation has been ";
	if ($geoEnabled) {
		$msg .= "enabled";
	} else {
		$msg .= "disabled";
	}
	
	/*$varName = 'useragent_output_available_browsers_and_platforms';
	$enabled = (int) $values['useragent_match_browser'];
	$msg = "Browser and platform data will ";
	if (!$enabled) {
		$msg .= "not ";
	}
	$msg .= "be added to Drupal.settings.useragent object";*/
	
	drupal_set_message($msg);
	
	$msg = "The useragent jaavscript file will ";
	if ($minifEnabled) {
		$msg .= "be minified.This setting is ideal for production.";
	} else {
		$msg .= "not be minified. This setting is ideal for debugging.";
	}
	drupal_set_message($msg);
}

function useragent_settings_macroregions($form,&$form_state) {
	$form = array(
			'#validate' => array('useragent_settings_macroregions_validate'),
			'#submit' => array('useragent_settings_macroregions_submit')
	);

	$regions = useragent_macroregion_data();

	$regionLines = array();
	foreach ($regions as $key => $region) {
		$regionLines[] = $key.'|'.$region;
	}
	$strRegions = !empty($regionLines)? implode("\n",$regionLines) : "";
	$numLines = count($regionLines);

	$form['info'] = array(
			'#type' => 'markup',
			'#markup' => "<p>".t("Macro-regions may be any arbitrary group of countries. These are then mapped to countries in the country settings form.")."</p>",
	);

	$form['macroregions'] = array(
			'#type' => 'textarea',
			'#title' => 'Macro-regions',
			'#default_value' => $strRegions,
			'#cols' => 40,
			'#rows' => $numLines + 5,
			'#description' => t("Each line requires a three-letter region code followed by a pipe and the display name of the macro-region.")
	);
	
	$form['save'] = array(
			'#type' => 'submit',
			'#value' => 'Save',
			'#id' => 'edit-save'
	);
	$form['restore'] = array(
			'#type' => 'submit',
			'#value' => 'Restore default values',
			'#id' => 'edit-restore'
	);
	return $form;
}

function useragent_settings_macroregions_validate($form,&$form_state) {
	_useragent_settings_macroregions_parse_values($form,$form_state,'validate');
}

function _useragent_settings_macroregions_parse_values($form,&$form_state,$mode = 'validate') {
	$values = $form_state['values'];
	if (isset($values['macroregions'])) {
		$regData = array();
		$strRegions = $values['macroregions'];
		$strRegions = trim($strRegions);
		if (!empty($strRegions) || strlen($strRegions)>4) {
			$regions = explode("\n",$strRegions);
			$lineNo = 0;
			foreach ($regions as $regLine) {
				$lineNo++;
				if (!empty($regLine) && strlen($regLine)> 4) {
					$items = explode('|',$regLine);
					$code = strtoupper(trim($items[0]));
					$code = preg_replace('#[^A-Z0-9]+#i','',$code);
					$name = '';
					if (isset($items[1])) {
						$name = preg_replace('#[\.;,:]+$#','',trim($items[1]));
					}
					$validCode = (is_string($code) && strlen($code) == 3);
					$validName = (is_string($name) && strlen($name) > 2);
					if ($mode == 'validate' && (!$validCode || !$validName)) {
						$params = array('@num' => $lineNo);
						form_set_error("macroregions",t("Line @num needs a three letter code separated by a pipe and a display name with at least two characters",$params));
					}
					if ($mode == 'submit' && $validCode && $validName) {
						$regData[$code] = $name;
					}
				}
			}
		}
		if ($mode == 'submit' && !empty($regData)) {
			drupal_set_message("Macro-regions saved");
			variable_set('useragent_macroregions',$regData);
		}
	}
}

function useragent_settings_macroregions_submit($form,&$form_state) {
	_useragent_settings_macroregions_parse_values($form,$form_state,'submit');
}

function useragent_settings_countries($form,&$form_state) {
	$form = array(
			'#submit' => array('useragent_settings_countries_submit')
	);
	
	$regions = useragent_macroregion_data();
	
	$regionOpts = array(
			'' => t('Select a macro-region')
	);
	foreach ($regions as $key => $region) {
		$regionOpts[$key] = $region;
	}

	$countries = useragent_country_data();
	
	$form['info'] = array(
			'#type' => 'markup',
			'#markup' => "<p>".t("You may assign the countries listed here to the macro-regions configurable in the macro-regions settings form.")."</p>",
	);

	$form['countries'] = array(
			'#type' => 'fieldset',
			'#title' => 'Countries',
			'#tree' => TRUE,
	);
	
	foreach ($countries as $key => $country) {
		$country = (object) $country;
		
		$form['countries'][$key] = array(
				'#type' => 'fieldset',
				'#title' => $country->name,
				'#tree' => TRUE,
		);
		$form['countries'][$key]['regcode'] = array(
				'#type' => 'select',
				'#title' => t("Macro-region"),
				'#options' => $regionOpts,
				'#default_value' => $country->regcode,
		);
		
		$form['countries'][$key]['name'] = array(
				'#type' => 'textfield',
				'#title' => t("Display name"),
				'#size' => 80,
				'#default_value' => $country->name,
		);
	}
	$form['save'] = array(
			'#type' => 'submit',
			'#value' => 'Save',
			'#id' => 'edit-save'
	);
	$form['restore'] = array(
			'#type' => 'submit',
			'#value' => 'Restore default values',
			'#id' => 'edit-restore'
	);
	return $form;
}

function useragent_settings_countries_submit($form,&$form_state) {
	$values = $form_state['values'];
	$countries = $values['countries'];
	variable_set('useragent_countries', $countries);
	drupal_set_message("Country names and macro-region mapping saved");
	cache_clear_all('useragent:geodata_','cache',TRUE);
	cache_clear_all('useragent:ip_geo','cache',TRUE);
}

function useragent_settings_screensizes($form,&$form_state) {
	$form = array(
			'#validate' => array('useragent_settings_screensizes_validate'),
			'#submit' => array('useragent_settings_screensizes_submit')
	);

	$sizes = useragent_screen_sizes();

	$numSizes = is_array($sizes)? count($sizes) : 0;
	
	$descr = t("Each size field specifies the minimum width x height (e.g. 800x600) that the detected broweser view port must meet.");
	$descr .= t("The matched screen size will always be the largest available size for the browser's view port. This corresponds to the adjusted pixel width and height of the screen area occupied by the browser window.");
	
	$form['screensizes'] = array(
		'#type' => 'fieldset',
		'#title' => 'Screen sizes',
		'#tree' => TRUE,
		'#default_value' => $strSizes,
		'#description' => $descr
	);
	
	foreach ($sizes as $key => $dimensions) {
		if (useragent_screensize_dimensions_valid($dimensions)) {
			$dimensions[0] = (int) $dimensions[0];
			$dimensions[1] = (int) $dimensions[1];
			$value = $dimensions[0].'x'.$dimensions[1];
			$form['screensizes'][$key] = array(
					'#type' => 'textfield',
					'#title' => $key,
					'#default_value' => $value,
					'#size' => 12,
			);
		}
	}
	
	
	$form['save'] = array(
			'#type' => 'submit',
			'#value' => 'Save',
			'#id' => 'edit-save'
	);
	$form['restore'] = array(
			'#type' => 'submit',
			'#value' => 'Restore default values',
			'#id' => 'edit-restore'
	);
	return $form;
}

function useragent_settings_screensizes_validate($form,&$form_state) {
	_useragent_settings_screensizes_parse_values($form,$form_state,'validate');
}

function useragent_settings_screensizes_submit($form,&$form_state) {
	_useragent_settings_screensizes_parse_values($form,$form_state,'submit');
}

function _useragent_settings_screensizes_parse_values($form,&$form_state,$mode = 'validate') {
	$values = $form_state['values'];
	if (isset($values['screensizes'])) {
		$data = array();
		$sizes = $values['screensizes'];
		if (!empty($sizes) && is_array($sizes)) {
			foreach ($sizes as $key => $size) {
				$valid = false;
				if (!empty($size) && strlen($size)> 2) {
					$dimensions = explode('x',$size);
					if (useragent_screensize_dimensions_valid($dimensions)) {
						$data[$key] = $dimensions;
						$valid = true;
						if ($mode == 'submit') {
							$data[$key] = $dimensions;
						}
					} 
				}
				if (!$valid && $mode == 'validate') {
					$params = array('@key' => $key);
					form_set_error("screensizes[".$key."]",t("The @key field needs a numeric width and height in the format: 800x600 ",$params));
				}
			}
		}
		if ($mode == 'submit' && !empty($data)) {
			drupal_set_message("Screen sizes saved");
			variable_set('useragent_screen_sizes',$data);
		}
	}
}

function useragent_screensize_dimensions_valid($dimensions = array()) {
	return (is_array($dimensions) && count($dimensions)==2 && is_numeric($dimensions[0]) &7 && is_numeric($dimensions[1]));
}

function useragent_settings_timesofday($form,&$form_state) {
	$form = array(
			'#validate' => array('useragent_settings_timesofday_validate'),
			'#submit' => array('useragent_settings_timesofday_submit')
	);

	$times = useragent_timesofday();

	$timeLines = array();
	if (!empty($times)) {
		foreach ($times as $key => $hour) {
			if (is_numeric($hour) && $hour < 24 && $hour >= 0) {
				$hour = (int) $hour;
				$timeLines[] = $key.'|'.$hour;
			}
		}
	}
	$strTimes = !empty($timeLines)? implode("\n",$timeLines) : "";
	$numLines = count($timeLines);
	$form['timesofday'] = array(
			'#type' => 'textarea',
			'#title' => 'Screen sizes',
			'#default_value' => $strTimes,
			'#cols' => 20,
			'#rows' => ($numLines + 2),
			'#description' => t("Each period requires a lower case machine name, e.g. night followed by its hour of commencement using the 24 hour clock, where 0 means midnight and 23 the last hour of the day. The next value after 23 is 0, so if one period starts at 22, it will end the hour before first period.")
	);
	$form['save'] = array(
			'#type' => 'submit',
			'#value' => 'Save',
			'#id' => 'edit-save'
	);
	$form['restore'] = array(
			'#type' => 'submit',
			'#value' => 'Restore default values',
			'#id' => 'edit-restore'
	);
	return $form;
}

function useragent_settings_timesofday_validate($form,&$form_state) {
	_useragent_settings_timesofday_parse_values($form,$form_state,'validate');
}

function useragent_settings_timesofday_submit($form,&$form_state) {
	_useragent_settings_timesofday_parse_values($form,$form_state,'submit');
}

function _useragent_settings_timesofday_parse_values($form,&$form_state,$mode = 'validate') {
	$values = $form_state['values'];
	if (isset($values['timesofday'])) {
		$data = array();
		$strTimes = $values['timesofday'];
		$strTimes = trim($strTimes);
		if (!empty($strTimes) || strlen($strTimes)>3) {
			$timeLines = explode("\n",$strTimes);
			$lineNo = 0;
			foreach ($timeLines as $line) {
				$lineNo++;
				if (!empty($line) && strlen($line)> 2) {
					$items = explode('|',$line);
					$period = strtolower(trim($items[0]));
					$period = preg_replace('#[^a-z0-9]+#i','',$period);
					$hour = -1;
					if (isset($items[1])) {
						$hour = (int) preg_replace('#[^0-9]#','',trim($items[1]));
					}
					$validPeriod = (is_string($period) && strlen($period)>1);
					$validHour = ($hour >= 0 && $hour < 24);
					if ($mode == 'validate' && (!$validPeriod || !$validHour)) {
						$params = array('@num' => $lineNo);
						form_set_error("timesofday",t("Line @num needs a period word separated by a pipe and a start hour between 0 and 23",$params));
					}
					if ($mode == 'submit' && $validPeriod && $validHour) {
						$data[$period] = $hour;
					}
				}
			}
			if (!empty($data)) {
				asort($data);
			}
		}
		if ($mode == 'submit' && !empty($data)) {
			drupal_set_message("Times of day have been saved");
			variable_set('useragent_timesofday',$data);
		}
	}
}

function useragent_settings_browser_types($form,&$form_state) {
	$form = array(
			'#submit' => array('useragent_settings_browser_types_submit')
	);

	$items = useragent_browser_types();
	$itemLines = array();
	$numItems = 0;
	if (!empty($items)) {
		foreach ($items as $item) {
			if (is_string($item)) {
				$item = trim($item);
				if (strlen($item)>2) {
					$itemLines[] = $item;
					$numItems++;
				}
			}
		}
	}
	$numRows = ($numItems + 2);
	if ($numRows < 6) {
		$numRows = 6;
	}
	$strItems = !empty($itemLines)? implode("\n",$itemLines) : "";
	$form['browser_types'] = array(
			'#type' => 'textarea',
			'#title' => 'Browser types',
			'#default_value' => $strItems,
			'#cols' => 20,
			'#rows' => $numRows,
			'#description' => t("Please add one browser keyword per line. These keywords must match the most specific for a given browser. Some keywords will appear in many browsers, e.g. for historical reasons all browsers include the keyword <em>Mozilla</em> and Webkit / KHTML browsers also refer to <em>Gecko</em>, while Blackberry and Android browsers refer to <em>Safari.</em>. Remember, IE is identified by MSIE. The javascript routine will return the first match, so if a user agent string includes the words <em>Fennec</em> and <em>Firefox</em>, we need to match <em>Fennec</em> first to ensure we have the mobile version. Likewise the Chrome user agent string includes a reference to Safari.")
	);
	$form['save'] = array(
			'#type' => 'submit',
			'#value' => 'Save',
			'#id' => 'edit-save'
	);
	$form['restore'] = array(
			'#type' => 'submit',
			'#value' => 'Restore default values',
			'#id' => 'edit-restore'
	);
	return $form;
}

function useragent_settings_browser_types_submit($form,&$form_state) {
	_useragent_settings_textarea_submit($form,$form_state,'browser_types','Browser types');
}

function useragent_settings_platforms($form,&$form_state) {
	$form = array(
			'#submit' => array('useragent_settings_platforms_submit')
	);

	$items = useragent_platforms();

	$itemLines = array();
	$numItems = 0;
	if (!empty($items)) {
		foreach ($items as $item) {
			if (is_string($item)) {
				$item = trim($item);
				if (strlen($item)>2) {
					$itemLines[] = $item;
					$numItems++;
				}
			}
		}
	}
	$numRows = ($numItems + 2);
	if ($numRows < 6) {
		$numRows = 6;
	}
	$strItems = !empty($itemLines)? implode("\n",$itemLines) : "";
	$form['platforms'] = array(
			'#type' => 'textarea',
			'#title' => 'Browser types',
			'#default_value' => $strItems,
			'#cols' => 20,
			'#rows' => $numRows,
			'#description' => t("Please add one platform (Operating System Device category) keyword per line. These keywords must match the most specific for a given platform. Add the most specific matches first. It is important to distinguish mobile platform from desktop and tablet relatives. For reason in the default platform list, <em>Windows Phone</em> precedes <em>Windows</em> and <em>iPad</em> precedes <em>iPhone</em> and <em>Android</em> precedes <em>Linux</em>.")
	);
	$form['save'] = array(
			'#type' => 'submit',
			'#value' => 'Save',
			'#id' => 'edit-save'
	);
	$form['restore'] = array(
			'#type' => 'submit',
			'#value' => 'Restore default values',
			'#id' => 'edit-restore'
	);
	return $form;
}

function useragent_settings_platforms_submit($form,&$form_state) {
	_useragent_settings_textarea_submit($form,$form_state,'platforms','Platforms (OSes and device types)');
}

function _useragent_settings_textarea_submit($form,&$form_state,$fieldName = 'browser_types',$label = 'Browser types') {
	$values = $form_state['values'];
	if (isset($values[$fieldName])) {
		$data = array();
		$strItems = $values[$fieldName];
		$strItems = trim($strItems);
		if (!empty($strItems) || strlen($strItems)>4) {
			$items = explode("\n",$strItems);
			foreach ($items as $item) {
				$item = trim($item);
				if (strlen($item)>1) {
					$data[] = $item;
				}
			}
		}
		if (!empty($data)) {
			drupal_set_message($label." saved");
			variable_set('useragent_'.$fieldName,$data);
		}
	}
}

function useragent_settings() {
	$variables = array(
			'useragent_enabled_themes' => 'array',

	);
}